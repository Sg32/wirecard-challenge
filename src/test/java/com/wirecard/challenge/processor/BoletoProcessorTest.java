package com.wirecard.challenge.processor;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wirecard.challenge.domain.Boleto;
import com.wirecard.challenge.domain.Payment;

public class BoletoProcessorTest {

	@Test
    public void process() {
		PaymentProcessor boletoProcessor = BoletoProcessor.newIntance();

		Payment payment = Payment.builder().build();
		
		String result = boletoProcessor.process(payment);
		
		assertEquals(Boleto.GENERATED_RESULT_NUMBER, result);
    }
	
}
