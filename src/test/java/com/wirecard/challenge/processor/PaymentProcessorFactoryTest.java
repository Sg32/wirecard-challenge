package com.wirecard.challenge.processor;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import com.wirecard.challenge.domain.PaymentType;

public class PaymentProcessorFactoryTest {

	@Test
    public void getBoletoProcessor() {
		PaymentProcessor boletoProcessor = PaymentProcessorFactory.getProcessor(PaymentType.BOLETO);

        assertThat(boletoProcessor).isInstanceOf(BoletoProcessor.class);
    }
	
	@Test
    public void getCreditCardProcessor() {
		PaymentProcessor boletoProcessor = PaymentProcessorFactory.getProcessor(PaymentType.CREDIT_CARD);

        assertThat(boletoProcessor).isInstanceOf(CreditCardProcessor.class);
    }
	
}