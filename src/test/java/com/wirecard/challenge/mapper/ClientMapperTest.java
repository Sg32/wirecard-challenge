package com.wirecard.challenge.mapper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.dto.ClientDTO;

public class ClientMapperTest {

	private static final Integer ID = 1;
	
	private ClientMapper clientMapper = ClientMapper.INSTANCE;
	
	@Test
	public void clientToClientDTO() {
		Client client = Client.builder()
				.id(ID)
				.build();
		
		ClientDTO clientDTO = clientMapper.clientToClientDTO(client);
		
		assertEquals(ID, clientDTO.getId());
	}
	
	@Test
	public void clientDTOToClient() {
		ClientDTO clientDTO = ClientDTO.builder()
				.id(ID)
				.build();
		
		Client client = clientMapper.clientDTOToClient(clientDTO);
		
		assertEquals(ID, client.getId());
	}
}