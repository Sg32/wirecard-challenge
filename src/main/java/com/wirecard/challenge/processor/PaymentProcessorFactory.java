package com.wirecard.challenge.processor;

import com.wirecard.challenge.domain.PaymentType;

public class PaymentProcessorFactory {

	private PaymentProcessorFactory() { }
	
	public static PaymentProcessor getProcessor(PaymentType paymentType) {
		PaymentProcessor paymentProcessor = null;
		switch (paymentType) {
			case BOLETO:
				paymentProcessor = BoletoProcessor.newIntance();
				break;
			case CREDIT_CARD:
				paymentProcessor = CreditCardProcessor.newIntance();
				break;
			default:
				throw new IllegalArgumentException();
		}
		return paymentProcessor;
	}
	
}
