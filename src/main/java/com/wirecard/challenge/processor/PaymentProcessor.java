package com.wirecard.challenge.processor;

import com.wirecard.challenge.domain.Payment;

public interface PaymentProcessor {

	public String process(Payment payment);
	
}