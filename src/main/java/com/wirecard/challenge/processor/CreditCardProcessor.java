package com.wirecard.challenge.processor;

import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;

import br.com.moip.validators.CreditCard;

public class CreditCardProcessor implements PaymentProcessor {

	private CreditCardProcessor() { }
	
	public static CreditCardProcessor newIntance() {
		return new CreditCardProcessor();
	}
	
	@Override
	public String process(Payment payment) {
		PaymentStatus status = processStatus(payment.getCard());
		payment.setStatus(status);
		return status.getDescription();
	}

	private PaymentStatus processStatus(Card card) {
		CreditCard creditCard = new CreditCard(card.getNumber());
		PaymentStatus status = PaymentStatus.DENIED;
		if (creditCard.isValid()) {
			status = PaymentStatus.APPROVED;
		}
		return status;
	}

}