package com.wirecard.challenge.processor;

import com.wirecard.challenge.domain.Boleto;
import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.domain.PaymentStatus;

public class BoletoProcessor implements PaymentProcessor {

	private BoletoProcessor() { }
	
	public static BoletoProcessor newIntance() {
		return new BoletoProcessor();
	}
	
	@Override
	public String process(Payment payment) {
		Boleto boleto = Boleto.builder()
			.number(Boleto.GENERATED_RESULT_NUMBER)
			.build();
		payment.setBoleto(boleto);
		payment.setStatus(PaymentStatus.APPROVED);
		return boleto.getNumber();
	}
	
}