package com.wirecard.challenge.service;

import org.springframework.stereotype.Service;

import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.dto.PaymentDTO;
import com.wirecard.challenge.exception.ResourceNotFoundException;
import com.wirecard.challenge.mapper.PaymentMapper;
import com.wirecard.challenge.processor.PaymentProcessorFactory;
import com.wirecard.challenge.repository.PaymentRepository;

@Service
public class PaymentService {
	
	private final PaymentMapper paymentMapper;
	private final PaymentRepository paymentRepository;
	
	public PaymentService(PaymentMapper paymentMapper, PaymentRepository paymentRepository) {
		this.paymentMapper = paymentMapper;
		this.paymentRepository = paymentRepository;
	}
	
	public PaymentDTO findById(Integer id) {
		return paymentMapper.paymentToPaymentDTO(paymentRepository
				.findById(id)
				.orElseThrow(ResourceNotFoundException::new));
	}

	public String create(PaymentDTO paymentDTO) {
		Payment payment = paymentMapper.paymentDTOToPayment(paymentDTO);
		String result = PaymentProcessorFactory
				.getProcessor(payment.getType())
				.process(payment);
		paymentRepository.save(payment);
		return result;
	}

}