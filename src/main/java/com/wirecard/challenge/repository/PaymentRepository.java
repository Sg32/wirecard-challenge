package com.wirecard.challenge.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wirecard.challenge.domain.Payment;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {

}