package com.wirecard.challenge.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardDTO {

	private Integer id;
	
	private String holderName;
	
	private String number;
	
	private String expirationDate;
	
	private String cvv;
	
}