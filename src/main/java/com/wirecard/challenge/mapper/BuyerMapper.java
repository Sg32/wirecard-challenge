package com.wirecard.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.wirecard.challenge.domain.Buyer;
import com.wirecard.challenge.dto.BuyerDTO;

@Mapper
public interface BuyerMapper {

	public BuyerMapper INSTANCE = Mappers.getMapper(BuyerMapper.class);
	
	public BuyerDTO buyerToBuyerDTO(Buyer buyer);
	
	public Buyer buyerDTOToBuyer(BuyerDTO buyerDTO);
	
}