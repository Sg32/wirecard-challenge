package com.wirecard.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.wirecard.challenge.domain.Card;
import com.wirecard.challenge.dto.CardDTO;

@Mapper
public interface CardMapper {

	public CardMapper INSTANCE = Mappers.getMapper(CardMapper.class);
	
	public CardDTO cardToCardDTO(Card card);
	
	public Card cardDTOToCard(CardDTO cardDTO);
	
}