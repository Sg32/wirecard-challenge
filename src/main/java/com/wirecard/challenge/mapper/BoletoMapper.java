package com.wirecard.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.wirecard.challenge.domain.Boleto;
import com.wirecard.challenge.dto.BoletoDTO;

@Mapper
public interface BoletoMapper {

	public BoletoMapper INSTANCE = Mappers.getMapper(BoletoMapper.class);
	
	public BoletoDTO boletoToBoletoDTO(Boleto boleto);
	
	public Boleto boletoDTOToBoleto(BoletoDTO boletoDTO);
	
}