package com.wirecard.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.wirecard.challenge.domain.Payment;
import com.wirecard.challenge.dto.PaymentDTO;

@Mapper
public interface PaymentMapper {

	public PaymentMapper INSTANCE = Mappers.getMapper(PaymentMapper.class);
	
	public PaymentDTO paymentToPaymentDTO(Payment payment);
	
	public Payment paymentDTOToPayment(PaymentDTO paymentDTO);
	
}