package com.wirecard.challenge.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import com.wirecard.challenge.domain.Client;
import com.wirecard.challenge.dto.ClientDTO;

@Mapper
public interface ClientMapper {

	public ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);
	
	public ClientDTO clientToClientDTO(Client client);
	
	public Client clientDTOToClient(ClientDTO clientDTO);
	
}